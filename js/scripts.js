$(document).ready(function() {

	$('.fireworks-trigger').on('click', function() {
		$(this).hide();
		$('.pyro').show();
		$('.greetings-container').removeClass('hidden');
		setTimeout(function() {
			$('.grad-pic-1').removeClass('hidden').addClass('animated jackInTheBox');

			setTimeout(function() {
				$('.grad-pic-2').removeClass('hidden').addClass('animated jackInTheBox');

				setTimeout(function() {
					$('.grad-pic').removeClass('animated');
					$('.sp-container').removeClass('hidden');
				}, 700);
			}, 500);
		}, 500);
	});

	lightGallery(document.getElementById('lightgallery'));

});